CC=icpc

SOURCES=main.cpp lbcoef.cpp smectic.cpp 
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=smectic

INTEL_LIBDIR= -L$(INTEL_PATH)/complier/lib/intel64
INTEL_LIBS= -limf -lintlc -lirng -lsvml -mkl

CFLAGS   = -c -Wall -openmp -std=c++11
CPPFLAGS =
LDFLAGS  = $(INTEL_LIBDIR)
LIBS     = -openmp $(INTEL_LIBS)

# intel math-kernel
MKL_INCLUDE_DIR=$(INTEL_PATH)/mkl/include
MKL_LIB_DIR=$(INTEL_PATH)/mkl/lib
MKL_LIBS= -mkl

CPPFLAGS +=-I$(MKL_INCLUDE_DIR)
LDFLAGS  +=-L$(MKL_LIB_DIR)
LIBS     += $(MKL_LIBS)

# mpich
MPI_DIR=$(CRAY_MPICH2_DIR)
MPI_LIB_DIR=-L$(CRAY_MPICH2_DIR)/lib
MPI_INCLUDE_DIR=-I$(CRAY_MPICH2_DIR)/include
MPI_LIBS=-lmpich

CPPFLAGS += $(MPI_INCLUDE_DIR)
LDFLAGS  += $(MPI_LIB_DIR)
LIBS     += $(MPI_LIBS)


all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS)
	$(CC) -o $@ $(LDFLAGS) $(OBJECTS) $(LIBS)

.cpp.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) $< 

smectic.o: parmesh.hpp

.PHONEY: clean

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)
