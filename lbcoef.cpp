#include <math.h>
#include "smectic.h"

void lbcoef_hex (int N, real_t A, real_t * gc11, real_t * gc22, real_t * gc12,
	real_t * sigma1, real_t * sigma2)
{

/* This function calculates the metric coeficients related with the
 * intrinsic geometry of the hexagonal substrate. 
 */

	real_t L =N/2.0;
	real_t Ly=2.0*L/sqrt(3.);
	real_t k=4.0*3.1416/L;
	real_t dx=L/N;
	real_t dy=Ly/N;
	real_t Aaux, Baux, Caux, Daux, Eaux;
	

	// Pre-calculate values that remain unchanged in loops
	real_t c1 = sqrt(3.0);
	//real_t c2 = 0.5 * c1;
	real_t c3 = A * A * k * k;
	real_t c4 = c3 * c3;

   	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
		{
			real_t kx = k*i*dx;
			real_t ky = k*j*dy;
			real_t a1 = 0.5*kx+0.5*c1*ky;
			real_t a2 = 0.5*kx-0.5*c1*ky;

			real_t sin1 = sin(kx);
			real_t sin2 = sin(a1);
			real_t sin3 = sin(a2);
			real_t sin4 = sin3 - sin2;

			real_t cos1 = cos(kx);
			real_t cos2 = cos(a1);
			real_t cos3 = cos(a2);

			real_t t1 = sin1 + 0.5*sin2 + 0.5*sin3;
			real_t t9 = t1*t1;
			real_t t2 = sin4 * sin4;
			real_t t3 = 0.5*cos3 - 0.5*cos2*k;
			real_t t4 = 0.25*c1*k*(cos2-cos3);
			real_t t6 = cos1*k+0.25*cos2*k+0.25*cos3*k;
			real_t t7 = 0.75*c4*t9*t2;
			real_t t8 = -0.5*cos2*c1*k-0.5*cos3*c1*k;
 
			real_t g11 = 1.0+c3*t9;
			real_t g12 = -0.5*c1*c3*t1*sin4;
			real_t g22 = 1.0+0.75*c3*t2;			
			real_t g  =  g11*g22 - g12*g12;	

			real_t tmp1 = g11 * g22;
			real_t tmp2 = pow (g11*g22-0.75*c4*t9*t2, 1.5);

			gc11[i * N + j]=g22/g; 
			gc22[i * N + j]=g11/g;
			gc12[i * N + j]=-g12/g;
  
			Aaux=1.5*c3*sin4*(-t3*k)/sqrt(tmp1-t7);

			Baux=-0.5*g22/pow(tmp1-t7,1.5)*(2.0*c3*t1*t6*g22+1.5*g11*c3*sin4*(-t3*k)-1.5*c4*t1*t2*t6-1.5*c4*t9*sin4*(-t3*k));

			Caux=0.5*c1*c3*t4*sin4/sqrt(tmp1-t7);
			Daux=0.5*c1*c3*t1*t8/sqrt((1+c3*t9)*g22-t7);

			Eaux=-0.25*c1*c3*t1*sin4/tmp2*(2.0*c3*t1*t4*g22+1.5*g11*c3*sin4*t8-1.5*c4*t1*t2*t4-1.5*c4*t9*sin4*t8);

			sigma1[i * N + j]=(Aaux+Baux+Caux+Daux+Eaux)/sqrt(g);
    

			Aaux=0.5*c1*c3*t6*sin4/sqrt(tmp1-t7);

			Baux=0.5*c1*c3*t1*(-t3*k)/sqrt(tmp1-t7);

			Caux=-0.25*c1*c3*t1*sin4/pow(tmp1-t7,1.5)*(2.0*c3*t1*t6*g22+3/2*g11*c3*sin4*(-t3*k)-3/2*c4*t1*t2*t6-3/2*c4*t9*sin4*(-t3*k));

			Daux=2.0*c3*t1*t4/sqrt(g11*g22-t7);

			Eaux=-0.5*g11/tmp2*(2.0*c3*t1*t4*g22+1.5*g11*c3*sin4*t8-1.5*c4*t1*t2*t4-1.5*c4*t9*sin4*t8);

			sigma2[i * N + j]=(Aaux+Baux+Caux+Daux+Eaux)/sqrt(g);
		}
} 
