#include <fstream>
#include <iostream>
#include <cmath>
#include <pthread.h>
#include <mpi.h>

#if defined __INTEL_COMPILER
#include <mkl.h>
#include <mkl_vsl.h>
#elif defined __GNUC__
#include <random>
#endif

#include "smectic.h"
#include "parmesh.hpp"

/*! calculte square of the argument */
inline real_t pow2 (real_t v) {
    return (v * v);
}

/*! calculate cube of the argument */
inline real_t pow3 (real_t v) {
    return ( v * v * v);
}

/*! calculate step1 */ 
inline void calc_mapf (int N, real_t * h_coef, real_t * psi, real_t * mapf) {
    int i;
#pragma omp parallel for
    for (i = 0; i < N; i++) {
        real_t elm = psi[i];
        mapf[i] = h_coef[0] * elm + h_coef[1] * pow2 (elm) + h_coef[2] * pow3 (elm);
    }
}


/*! calculate psiaux */
inline void calc_psiaux (int N, real_t * c11, real_t * c22, 
        real_t * c12, real_t * sigma1, real_t * sigma2, 
        real_t * dfdx, real_t * dfdy,
        real_t * dfdxx, real_t * dfdyy, real_t * dfdxy, 
        real_t * psiaux) {

    int i;
#pragma omp parallel for
    for (i = 0; i < N; i++)  
        psiaux[i] = c11[i] * dfdxx[i] + c22[i] * dfdyy[i] 
            + 2 * c12[i] * dfdxy[i] 
            + sigma1[i] * dfdx[i] + sigma2[i] * dfdy[i];
}

/*! update psiaux */
inline void update_psiaux (int N, real_t D, real_t * mapf, real_t * psiaux) {
    int i;
#pragma omp parallel for
    for (i = 0; i < N; i++)
        psiaux[i] = mapf[i] - D * psiaux[i];
}

/*! update psi */
void update_psi (int N, real_t dt, real_t M, real_t b, 
        real_t * psiaux, real_t * psi) {
    int i;
#pragma omp parallel for
    for (i = 0; i < N; i++)
        psi[i] += dt * ( M * psiaux[i] - b * psi[i]);
}


/*! computes 1st an 2nd derivatives using finite-differnces */
void cgradient (int Nx, int Ny, real_t * dinv, real_t * f, 
        real_t * dfdx, real_t * dfdy, real_t * dfdxx, real_t * dfdyy) {

    int i, j;
    real_t ** f2d = reshape(f, Nx, Ny);
#pragma omp parallel for collapse(2)
    for (i = 1; i < Nx-1; i++) {
        for (j = 1; j < Ny-1; j++) { 
            int idx = (i-1) * (Ny-2)  + (j-1);
            dfdx[idx] =  dinv[0] * (f2d[i-1][j] - f2d[i+1][j]);
            dfdy[idx] =  dinv[1] * (f2d[i][j-1] - f2d[i][j+1]);
            dfdxx[idx] = dinv[2] * (f2d[i+1][j] + f2d[i-1][j] - 2 * f2d[i][j]);
            dfdyy[idx] = dinv[3] * (f2d[i][j+1] + f2d[i][j-1] - 2 * f2d[i][j]);
        }
    }
    freemat(f2d);
}

void cgradient2 (int Nx, int Ny, real_t dinv, real_t * f, real_t * fx) {
    real_t ** f2d = reshape (f, Nx, Ny);
#pragma omp parallel for collapse(2)
    for (int i = 1; i < Nx-1; i++) {
        for (int j = 1; j < Ny-1; j++) {
            int idx = (i-1) * (Ny-2)  + (j-1);
            fx[idx] = dinv * (f2d[i-1][j] - f2d[i+1][j]);
        }
    }
    freemat(f2d);
}


void smecticOnHex (simParams_t params,
        real_t * gC11, real_t * gC22, real_t * gC12, 
        real_t * gSigma1, real_t * gSigma2) {

    int N = params.Nsize_;
    real_t Lx = N / 2.0;
    real_t Ly = 2.0 * Lx / sqrt(3);
    int Nmax = N * N;

    // read parameters
    real_t a, f, u, v, D, b, eta, M;
    std::ifstream in ("config.in");
    if (!in.is_open ()) {
        //std::cerr << "Warning: config file not found. Using default values." << std::endl;
        a = 1.5;
        f = 0.5;
        v = 2.3;
        u = 0.38;
        D = 0.3;
        b = 0.03;
        eta = 0.01;
        M = 1.0;
    } else { // TODO: to be replaced by a more friendly config file format
        in >> a;
        in >> f;
        in >> u;
        in >> v;
        in >> D;
        in >> b;
        in >> eta;
        in >> M;
    }
    in.close();

    real_t h_coef[3];
    h_coef[0] = - params.tau_ + a * ( 1 - 2 * f);
    h_coef[1] = v * ( 1 - 2 * f);
    h_coef[2] = u;

    real_t dinv[4];
    real_t dx = Lx / (real_t) N;
    real_t dy = Ly / (real_t) N;
    dinv[0] = 0.5 / dx;
    dinv[1] = 0.5 / dy;
    dinv[2] = 1.0 / dx / dx;
    dinv[3] = 1.0 / dy / dy;

    /* Parallelize stuff */
    ParMesh mesh (N, N);
    int ibeg, jbeg, nrow, ncol;
    mesh.get_loc_indices (ibeg, jbeg, nrow, ncol);
    Nmax = nrow * ncol;
    
    // copy local constant data
    real_t * c11 = new real_t[Nmax];
    real_t * c22 = new real_t[Nmax];
    real_t * c12 = new real_t[Nmax];
    real_t * sigma1 = new real_t[Nmax];
    real_t * sigma2 = new real_t[Nmax];

    int k = 0;
    for (int i = ibeg; i < ibeg + nrow; i++)
        for (int j = jbeg; j < jbeg + ncol; j++)
            c11[k++] = gC11[i * N + j];
    k = 0;
    for (int i = ibeg; i < ibeg + nrow; i++)
        for (int j = jbeg; j < jbeg + ncol; j++)
            c22[k++] = gC22[i * N + j];
    k = 0;
    for (int i = ibeg; i < ibeg + nrow; i++)
        for (int j = jbeg; j < jbeg + ncol; j++)
            c12[k++] = gC12[i * N + j];
    k = 0;
    for (int i = ibeg; i < ibeg + nrow; i++)
        for (int j = jbeg; j < jbeg + ncol; j++)
            sigma1[k++] = gSigma1[i * N + j];
    k = 0;
    for (int i = ibeg; i < ibeg + nrow; i++)
        for (int j = jbeg; j < jbeg + ncol; j++)
            sigma2[k++] = gSigma2[i * N + j];


    // allocate truckload of memory 
    real_t * psi = new real_t [Nmax];
    real_t * old = new real_t [Nmax];
    real_t * err = new real_t [Nmax];
    real_t * psiaux = new real_t [Nmax];
    real_t * mapf = new real_t [Nmax];
    real_t * fx = new real_t [Nmax];
    real_t * fy = new real_t [Nmax];
    real_t * fxx = new real_t [Nmax];
    real_t * fyy = new real_t [Nmax];
    real_t * fxy = new real_t [Nmax];
    real_t * val = new real_t [(nrow+2)*(ncol+2)];
     

    // set Psi to random numbers
#if defined __INTEL_COMPILER
    VSLStreamStatePtr stream;
    int seed = mesh.get_rank();
    vslNewStream(&stream, VSL_BRNG_MRG32K3A, seed);
    vdRngUniform(VSL_METHOD_DUNIFORM_STD, stream, Nmax, psi, 0., 1.);
    vslDeleteStream(&stream); 
#elif defined __GNUC__
    std::default_random_engine rand(mesh.get_rank());
    std::uniform_real_distribution<real_t> dist (0., 1.);
    for (int i = 0; i < Nmax; i++)
        psi[i] = dist(rand);
#endif 

    // time stepping
    real_t e = 1.0E+10;
    for (int i = 0; i < Nmax; i++) old[i] = 0.;
    for ( int istep = 0; istep < params.Nsteps_; istep++ ) {
        mesh.copy (psi);
        mesh.sync_start();

        // calcu mapf while ghosts are beign exhanged
        calc_mapf(Nmax, h_coef, psi, mapf);

        // wait for ghost-exchange to finish
        mesh.sync_end();
        mesh.get_global(val);
        cgradient(nrow+2, ncol+2, dinv, val, fx, fy, fxx, fyy);

        // calculate cross derivative
        mesh.copy(fy);
        mesh.sync_start();
        mesh.sync_end();
        mesh.get_global(val);
        cgradient2(nrow+2, ncol+2, dinv[0], val, fxy);

        // local operations
        calc_psiaux(Nmax, c11, c22, c12, sigma1, sigma2, fx, fy, fxx, fyy, fxy, psiaux);
        update_psiaux(Nmax, D, mapf, psiaux);

        mesh.copy (psiaux);
        mesh.sync_start();
        mesh.sync_end();
        mesh.get_global(val);
        cgradient(nrow+2, ncol+2, dinv, val, fx, fy, fxx, fyy);

        // cross-derivative
        mesh.copy(fy);
        mesh.sync_start();
        mesh.sync_end();
        mesh.get_global(val);
        cgradient2(nrow+2, ncol+2, dinv[0], val, fxy);

        // local operations
        calc_psiaux(Nmax, c11, c22, c12, sigma1, sigma2, fx, fy, fxx, fyy, fxy, psiaux);
        update_psi(Nmax, params.dt_, M, b, psiaux, psi);

        if ( istep > 0 && istep % 10000 == 0 ) {
            mesh.write(psi);
            for (int i = 0; i < Nmax; i++) err[i] = old[i] - psi[i];
            for (int i = 0; i < Nmax; i++) old[i] = psi[i];
            e = mesh.norm2(err);
            if (mesh.get_rank() == 0)
                std::cout<< "Error after "<< istep << " steps = " << e << std::endl;
            if ( e < 1.0E-10 ) break;
        }
    }
    mesh.write(psi);
    // print compute time on screen

    /* clean up */
    delete [] c11;
    delete [] c22;
    delete [] c12;
    delete [] sigma1;
    delete [] sigma2;
    delete [] old;
    delete [] err;
    delete [] psi;
    delete [] psiaux;
    delete [] mapf;
    delete [] fx;
    delete [] fy;
    delete [] fxx;
    delete [] fyy;
    delete [] fxy;
    delete [] val;
}
