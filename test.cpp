#include <iostream>
#include <fstream>

#include <mpi.h>
#include "parmesh.hpp"

const double PI = 3.14159265359;
const int N = 128;

double ** reshape (double * f, int nrow, int ncol) {
    double ** f2d = new double * [nrow];
    for (int i = 0; i < nrow; i++)
        f2d[i] = f + i * ncol;
    return f2d;
}

void freemat (double **f) {
    delete [] f;
}


int main(int argc, char ** argv) {
    int i, j, k;
    MPI_Init(&argc, &argv);
    ParMesh mesh (N, N);
    mesh.print(0);
    int ibeg, jbeg, nrow, ncol;
    mesh.get_loc_indices(ibeg, jbeg, nrow, ncol);
    double * f = new double[nrow * ncol];
    double * df = new double[nrow * ncol];
    double dx = PI/(N-1);
    k = 0;
    for (i = ibeg; i < ibeg + nrow; i++)
        for (j = jbeg; j < jbeg + ncol; j++) {
            double x = (double) i * dx;
            double y = (double) j * dx;
            f[k++] = sin(x) * cos(y);
        } 

    // do something with the data
    double dinv = 0.5 / dx;
    mesh.copy (f);
    mesh.sync_start();
    mesh.sync_end();
    double * var = new double[(nrow+2)*(ncol+2)];
    mesh.get_global(var);
    double ** f2d = reshape(var, nrow+2, ncol+2);
    for (i = 1; i < nrow+1; i++)
        for (j = 1; j < ncol+1; j++)
            df[(i-1)*ncol+(j-1)] = dinv * (f2d[i-1][j] - f2d[i+1][j]);
    freemat(f2d);
    delete [] var;

    // prepare to assemble the global matrix
    int nproc = mesh.get_comm_size();
    int * recvbuf1 = new int [4 * nproc];
    int sendbuf[4] = {ibeg, jbeg, nrow, ncol};
    MPI_Gather(sendbuf, 4, MPI_INT, recvbuf1, 4, MPI_INT, 0, MPI_COMM_WORLD);

    int * counts = new int [nproc];
    int * offset = new int [nproc];
    if (mesh.get_rank()==0) {
        counts[0] = nrow * ncol;
        offset[0] = 0;
        for (i = 1; i < nproc; i++) {
            counts[i] = recvbuf1[i * 4 + 2] * recvbuf1[i * 4 + 3];
            offset[i] = offset[i-1] + counts[i-1];
        } 
    }
    MPI_Barrier(MPI_COMM_WORLD);

    // Assemble matrix at the root
    double * recvbuf2 = new double [N * N];
    MPI_Gatherv(df, nrow * ncol, MPI_DOUBLE, recvbuf2, counts, offset, 
            MPI_DOUBLE, 0, MPI_COMM_WORLD);

    double * ff = NULL;
    if (mesh.get_rank() == 0) {
        ff = new double [N * N];
        for (int iproc = 0; iproc < nproc; iproc++) {
            ibeg = recvbuf1[4 * iproc + 0];
            jbeg = recvbuf1[4 * iproc + 1];
            nrow = recvbuf1[4 * iproc + 2];
            ncol = recvbuf1[4 * iproc + 3];
            k = 0;
            for (i = ibeg; i < ibeg + nrow; i++) {
                for (j = jbeg; j < jbeg + ncol; j++) {
                    ff[i * N + j] = recvbuf2[offset[iproc] + k++];
                }
            }
        }
        std::fstream out("matrix.dat", std::ios::out);
        for (i=0; i<N; i++){
            for (j=0; j<N; j++){
                out << ff[i*N+j] << " ";
            }
            out << std::endl;
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    delete [] f;
    delete [] df;
    delete [] recvbuf1;
    delete [] recvbuf2;
    delete [] counts;
    delete [] offset;
    if (ff) delete [] ff;
    return 0;
}
