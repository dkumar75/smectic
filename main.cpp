#include <iostream>
#include <fstream>
#include <cstdlib>

#include <mpi.h>
#include "smectic.h"

int main (int argc, char **argv)
{
    simParams_t params;
    params.Nsize_ = 256;
    params.Nsteps_ = 1000;
    params.tau_ = 0.2;
    params.dt_ = 0.0085;
    real_t Amp = 10;

    // start MPI
    MPI_Init (&argc, &argv);
    try {
        std::ifstream in ("parameters.in");
        if (in.is_open()) {
            in >> params.Nsize_;
            in >> params.Nsteps_;
            in >> Amp;
            in >> params.tau_;
            in >> params.dt_;
        }
        in.close();
    } catch (...) { 
        std::cerr << "Warning: could not find \"parameters.in\" file" << std::endl;
    }

    int Nsq = params.Nsize_ * params.Nsize_;
    /* allocate memory */
    real_t * c11 = new real_t [Nsq];
    real_t * c22 = new real_t [Nsq];
    real_t * c12 = new real_t [Nsq];
    real_t * sig1 = new real_t [Nsq];
    real_t * sig2 = new real_t [Nsq];

    // calculate 
    lbcoef_hex (params.Nsize_, Amp, c11, c22, c12, sig1, sig2);
    smecticOnHex (params, c11, c22, c12, sig1, sig2);

    delete [] c11;
    delete [] c22;
    delete [] c12;
    delete [] sig1;
    delete [] sig2;

    MPI_Finalize();
    return 0;
}
