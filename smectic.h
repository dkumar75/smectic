#ifndef SMECTIC__H
#define SMECTIC__H

#include <stddef.h>
#include <iostream>

#ifdef USE_SINGLE_PRECISION
typedef float real_t;
#else
typedef double real_t;
#endif

struct SimParams {
    int Nsize_;
    int Nsteps_;
    real_t tau_;
    real_t dt_;
};
typedef struct SimParams simParams_t;

struct OutputData {
    size_t N_;
    real_t * old_;
    real_t * cur_;
};
typedef struct OutputData outputData_t;


inline real_t ** reshape (real_t * data, int Nrow, int Ncol) {
    real_t ** mat = new (std::nothrow) real_t *[Nrow];
    for (int i = 0; i < Nrow; i++)
        mat[i] = data + i * Ncol;
    return mat;
}

inline void freemat (real_t ** mat) {
    delete [] mat;
}

void * write_output (void * );

void smecticOnHex (simParams_t , real_t * , real_t * , real_t * , 
        real_t * , real_t * );

void lbcoef_hex (int , real_t , real_t * , 
        real_t * , real_t * , real_t * , real_t * );

#endif // SMECTIC__H
