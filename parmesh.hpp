#ifndef __PARMESH_HPP__
#define __PARMESH_HPP__

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <mpi.h>

#ifdef USE_SINGLE_PRECISION
typedef float real_t;
#else
typedef double real_t;
#endif

class ParMesh {
    protected:
        int num_procs_; 
        int proc_id_;
        int nrows_; 
        int ncols_;
        int row_beg_;
        int col_beg_;
        int my_rows_;
        int my_cols_;
        real_t ** data_;

        // communication related stuff
        int neigh_proc_[4];
        int counts_[4];
        int offset_[4];
        MPI_Request send_req_[4];
        MPI_Request recv_req_[4];
        real_t * sendbuf_;
        real_t * recvbuf_;


    public:
        // constructor
        ParMesh (int nr, int nc) {
            nrows_ = nr;
            ncols_ = nc;

            // MPI size and rank
            MPI_Comm_size (MPI_COMM_WORLD, &num_procs_);
            MPI_Comm_rank (MPI_COMM_WORLD, &proc_id_); 

            // number of cuts in i and j direction
            int p_cols = (int) sqrt(num_procs_);
            while (num_procs_ % p_cols) p_cols--;
            int p_rows = num_procs_ / p_cols;
            //std::cerr << "p_rows = " << p_rows << ", p_cols = " << p_cols << std::endl;

            // rank as (i,j) of decomposition
            int p_i = proc_id_ / p_cols;
            int p_j = proc_id_ % p_cols;

            // rows and cols of local data
            my_rows_ = nrows_ / p_rows;
            int residual_i = nrows_ % p_rows;
            if (p_i < residual_i){
                my_rows_++;
                row_beg_ = my_rows_ * p_i;
            } else {
                row_beg_ = my_rows_ * p_i + residual_i;
            }
            //TODO add sanity checks
      
            my_cols_ = ncols_ / p_cols;
            int residual_j = ncols_ % my_cols_;
            if (p_j < residual_j){
                my_cols_++;
                col_beg_ = my_cols_ * p_j;
            } else {
                col_beg_ = my_cols_ * p_j + residual_j;
            }
            //TODO add sanity checks

            //allocate memory for local matrix + ghosts
            data_ = new real_t * [my_rows_+2];
            for (int i = 0; i < my_rows_+2; i++) {
                data_[i] = new real_t [my_cols_+2];
            }
        
            // get neigh procs
            if (p_i - 1 > -1)
                neigh_proc_[0] = (p_i-1) * p_cols + p_j;
            else
                neigh_proc_[0] = (p_rows-1) * p_cols + p_j;

            if (p_j - 1 > -1)
                neigh_proc_[1] = p_i * p_cols + p_j - 1;
            else
                neigh_proc_[1] = p_i * p_cols + p_cols - 1;

            if (p_i + 1 < p_rows)
                neigh_proc_[2] = (p_i+1) * p_cols + p_j;
            else
                neigh_proc_[2] = p_j;

            if (p_j + 1 < p_cols)
                neigh_proc_[3] = p_i * p_cols + p_j + 1;
            else
                neigh_proc_[3] = p_i * p_cols;
        
            // allocate memory to buffers
            size_t bufsize = 2 * (my_rows_ + my_cols_);
            sendbuf_ = new real_t [bufsize];
            recvbuf_ = new real_t [bufsize];

            // offsets and data counts
            offset_[0] = 0; 
            offset_[1] = my_cols_; 
            offset_[2] = my_cols_ + my_rows_;
            offset_[3] = 2 * my_cols_ + my_rows_;
            counts_[0] = my_cols_;
            counts_[1] = my_rows_;
            counts_[2] = my_cols_;
            counts_[3] = my_rows_;
        }

        // free all the allocated memory
        ~ParMesh() {
            delete [] sendbuf_;
            delete [] recvbuf_;
            for (int i = 0; i < my_rows_; i++)
                delete [] data_[i];
            delete [] data_;
        }

        // inliners
        inline int get_rank() const { return proc_id_; }
        inline int get_comm_size() const { return num_procs_; }

        // get local indices
        inline void get_loc_indices (int & ibeg, int & jbeg, int & nrows, int & ncols) const {
            ibeg = row_beg_;
            jbeg = col_beg_;
            nrows = my_rows_;
            ncols = my_cols_;
        }

        // copy data in
        void copy (real_t * f) {
            int i, j;
            for (i = 0; i < my_rows_+2; i++)
                for (j = 0; j < my_cols_+2; j++)
                    data_[i][j] = 0.;

            for (i = 1; i < my_rows_+1; i++)
                for (j = 1; j < my_cols_+1; j++)
                    data_[i][j] = f[(i-1) * my_cols_ + (j-1)];
        }

        // copy data with ghosts
        void get_global (real_t * f) {
            for (int i = 0; i < my_rows_+2; i++)
                for (int j = 0; j < my_cols_+2; j++)
                    f[i * (my_cols_ + 2) + j] = data_[i][j];
        }

        // copy data without ghosts
        void get_local (real_t * f) {
            for (int i = 1; i < my_rows_+1; i++)
                for (int j = 1; j < my_cols_+1; j++)
                    f[(i-1)*my_cols_+(j-1)] = data_[i][j];
        }

        // calculate NORM2 of the matrix
        real_t norm2(real_t * v) {
            real_t norm  = 0.;
            for (int i = 0; i < my_rows_; i++)
                for (int j = 0; j < my_cols_; j++)
                    norm += v[i * my_cols_ + j] * v[i * my_cols_ + j];

            real_t gnorm = 0.;            
#ifdef USE_SINGLE_PRECISION
            MPI_Datatype type = MPI_FLOAT;
#else
            MPI_Datatype type = MPI_DOUBLE;
#endif
            MPI_Allreduce (&norm, &gnorm, 1, type, MPI_SUM, MPI_COMM_WORLD);
            return gnorm;
        }

        // Start ghost exchange / non-blocking 
        void sync_start() {
            int i;
            int tag = 9883;
#ifdef USE_SINGLE_PRECISION
            MPI_Datatype type = MPI_FLOAT;
#else
            MPI_Datatype type = MPI_DOUBLE;
#endif
            // start receiving right away
            for (i = 0; i < 4; i++)
                MPI_Irecv (recvbuf_ + offset_[i], counts_[i], type, neigh_proc_[i], 
                        tag, MPI_COMM_WORLD, &recv_req_[i]);

            //pack and send buffers to neighbors
            for (i = 0; i < my_cols_; i++)
                sendbuf_[offset_[0] + i] = data_[1][i+1];

            for (i = 0; i < my_rows_; i++)
                sendbuf_[offset_[1] + i] = data_[i+1][1];

            for (i = 0; i < my_cols_; i++)
                sendbuf_[offset_[2] + i] = data_[my_rows_][i+1];

            for (i = 0; i < my_rows_; i++)
                sendbuf_[offset_[3] + i] = data_[i+1][my_cols_];

            for (i = 0; i < 4; i++)
                MPI_Isend (sendbuf_ + offset_[i], counts_[i], type, neigh_proc_[i], 
                        tag, MPI_COMM_WORLD, &send_req_[i]);
        }

        // ensures all the communications are finished / blocking
        void sync_end(){
            int i;
            // wait for all the in-coming communications to end
            MPI_Status status[4];
            MPI_Waitall (4, recv_req_, status);
            //TODO check status
            
            for (i = 0; i < my_cols_; i++)
                data_[0][i+1] = recvbuf_[offset_[0] + i];

            for (i = 0; i < my_rows_; i++)
                data_[i+1][0] = recvbuf_[offset_[1] + i];

            for (i = 0; i < my_cols_; i++)
                data_[my_rows_+1][i+1] = recvbuf_[offset_[2] + i];

            for (i = 0; i < my_rows_; i++)
                data_[i+1][my_cols_+1] = recvbuf_[offset_[3] + i];

            // Wait for all the out-going communications to finish 
            MPI_Waitall (4, send_req_, status);
            //TODO check for status
        }

        // Assemble data and write to disk
        void write (real_t * var) {
            int i, j, k;
            int * recvidx = new int [4 * num_procs_];
            int sendidx[4] = {row_beg_, col_beg_, my_rows_, my_cols_};
            MPI_Gather(sendidx, 4, MPI_INT, recvidx, 4, MPI_INT, 0, MPI_COMM_WORLD);

            int * cnt = NULL;
            int * dis = NULL;
            if (proc_id_ == 0) {
                cnt = new int [4 * num_procs_];
                dis = new int [4 * num_procs_];
                cnt[0] = my_rows_ * my_cols_;
                dis[0] = 0;
                for (i=1; i < num_procs_; i++) {
                    cnt[i] = recvidx[4 * i + 2] * recvidx[4 * i + 3];
                    dis[i] = dis[i-1] + cnt[i-1];
                }
            }
            MPI_Barrier(MPI_COMM_WORLD);

#ifdef USE_SINGLE_PRECISION
            MPI_Datatype type = MPI_FLOAT;
#else
            MPI_Datatype type = MPI_DOUBLE;
#endif

            real_t * buf = new real_t [nrows_ * ncols_];
            int sz = my_rows_ * my_cols_;
           
            MPI_Gatherv(var, sz, type, buf, cnt, dis, type, 0, MPI_COMM_WORLD);
            if (proc_id_ == 0) {
                real_t * ff = new real_t [nrows_ * ncols_];
                for (int iproc = 0; iproc < num_procs_; iproc++) {
                    int ibeg = recvidx[4 * iproc + 0];
                    int jbeg = recvidx[4 * iproc + 1];
                    int rows = recvidx[4 * iproc + 2];
                    int cols = recvidx[4 * iproc + 3];
                    k = 0;
                    for (i = ibeg; i < ibeg + rows; i++) {
                        for (j = jbeg; j < jbeg + cols; j++) {
                            ff[i * ncols_ + j] = buf[dis[iproc] + k++];
                        }
                    }
                }
                std::ofstream out("psi.out");
                if (!out.is_open()) {
                    std::cerr << "Error: unable to create new file." << std::endl;
                    exit(1);
                }
                for (i = 0; i < nrows_; i++) {
                    for (j = 0; j < ncols_; j++) {
                        out << ff[i * ncols_ + j] << " ";
                    }
                    out << std::endl;
                }
                out.close();
                delete [] ff;
            }
            MPI_Barrier (MPI_COMM_WORLD);
            delete [] buf;
            delete [] recvidx;
            if (cnt) delete [] cnt;
            if (dis) delete [] dis;
        }

        // debug and all that crap
        void print(int myid) const {
            if (proc_id_ == myid) {
                std::cout << "Comm size     : " << num_procs_ << std::endl;
                std::cout << "my proc id    : " << proc_id_ << std::endl;
                std::cout << "Global rows   : " << nrows_ << std::endl;
                std::cout << "Global cols   : " << ncols_ << std::endl;
                std::cout << "row beg       : " << row_beg_ << std::endl;
                std::cout << "col beg       : " << col_beg_ << std::endl;
                std::cout << "local rows    : " << my_rows_ << std::endl;
                std::cout << "local cols    : " << my_cols_ << std::endl;
                std::cout << "neigh_procs   : [  ";
                for (int i = 0; i < 4; i++)
                    std::cout << neigh_proc_[i] << " ";
                std::cout << " ]" << std::endl;
            }
        }
};

#endif // __PARMESH_HPP__
